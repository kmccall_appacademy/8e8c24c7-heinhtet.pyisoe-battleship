class BattleshipGame
  attr_reader :player, :board

  def initialize(player, board)
    @player = player
    @board = board
  end

  def count
    @board.count
  end

  def board(pos=[nil,nil])
    return @board if pos == [nil,nil]
    @board[pos[0]][pos[1]]
  end

  def play_turn
    puts "Welcome to battleship. Input coordinates to attack."
    puts "Please format inputs as row, col seperated by a comma"
    command = player.get_play
    self.attack(command)
  end

  def attack(pos)
    target = board[pos]
    if target == nil
      puts "Missed! Your enemy continues to elude you..."
      board[pos] = :x
    elsif target == :s
       puts "A direct hit! You've sunk an enemy battleship!"
       board[pos]  = :x
    else
       puts "Target area has already been bombed."
    end
  end

  def game_over?
    return true if @board.won?
    false
  end

end
