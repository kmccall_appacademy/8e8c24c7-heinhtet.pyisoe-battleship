class Board
    attr_accessor :grid
  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    @grid = Array.new(10) { Array.new(10)}
  end

  def [](pos)
   row, col = pos
   @grid[row][col]
  end

 def []=(pos, mark)
 row, col = pos
 @grid[row][col] = mark
 end

  def empty?(pos=nil)
    if pos == nil
        @grid.each {|innerarray| return false if innerarray.include?(:s) ||
        innerarray.include?(:x)}
        true
    else
      return true if @grid[pos[0]][pos[1]] == nil
      false
    end
  end

  def full?
    @grid.each do |innerarray|
      return false if innerarray.include?(nil)
    end
    true
  end


  def count
    counter = 0
     @grid.each do |innerarray|
      counter += innerarray.count(:s)
     end
    counter
  end

  def place_random_ship
      shiplaced = false
      until shiplaced
      raise "FULL GRID DETECTED" if self.full?
        row = rand(0..@grid.size-1)
        col = rand(0..@grid.size-1)
          if @grid[row][col] == nil
            @grid[row][col] = :s
            shiplaced = true
          end
      end
  end

  def won?
     @grid.each do |innerarray|
      return false if innerarray.include?(:s)
     end
    true
  end

end
