class HumanPlayer

  def initialize
    @player = self
  end

  def get_play
    input = $stdin.gets
    [input[0], input[1]]
  end

end
